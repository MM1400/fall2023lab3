package dawson;

import static org.junit.Assert.assertEquals;


import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */

    @Test
    public void echo_returns5() {
        assertEquals("testing if echo returns same value", 5, App.echo(5));
    }
    @Test
    public void oneMore_returns6() {
        assertEquals("testing if oneMore returns x++", 6, App.oneMore(5));
    }
}
